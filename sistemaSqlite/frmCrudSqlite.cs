﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO;

namespace sistemaSqlite
{
    public partial class frmCrudSqlite : Form
    {
        public class Personas
        {
            public int id { get; set; }
            public string nombre { get; set; }
        }
        private static string conexion = "Data Source=Banco.db";
        private static string nombreBanco = "Banco.db";
        private static int IDregistro;

        public frmCrudSqlite()
        {
            InitializeComponent();
        }

        private void frmCrudSqlite_Load(object sender, EventArgs e)
        {
            if(!File.Exists(nombreBanco))
            {
                SQLiteConnection.CreateFile(nombreBanco);
                SQLiteConnection conn = new SQLiteConnection(conexion);
                conn.Open();

                StringBuilder sql = new StringBuilder();
                sql.AppendLine("CREATE TABLE IF NOT EXISTS PERSONAS ([ID] INTEGER PRIMARY KEY AUTOINCREMENT,");
                sql.AppendLine("[NOMBRE] VARCHAR(50))");

                SQLiteCommand cmd = new SQLiteCommand(sql.ToString(), conn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al crear el banco de datos: " + ex.Message);
                }
            }

            Cargar();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(conexion);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO PERSONAS (NOMBRE) VALUES (@NOMBRE)", conn);
            cmd.Parameters.AddWithValue("NOMBRE",txtNombre.Text.Trim());
            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro guardado con éxito!");
                txtNombre.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar el registro: " + ex.Message);
            }

            Cargar();
        }

        private void Cargar()
        {
            SQLiteConnection conn = new SQLiteConnection(conexion);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM PERSONAS", conn);
            SQLiteDataReader dr = cmd.ExecuteReader();
            List<Personas> lista = new List<Personas>();
            while(dr.Read())
            {
                lista.Add(new Personas { 
                    id = Convert.ToInt32(dr["ID"]), 
                    nombre = dr["NOMBRE"].ToString() 
                });
            }
            dgvPersonas.DataSource = lista;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection(conexion);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM PERSONAS WHERE ID = @CODIGO", conn);
            cmd.Parameters.AddWithValue("CODIGO", Convert.ToInt32(dgvPersonas.CurrentRow.Cells[0].Value));

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Registro eliminado con éxito!");
                Cargar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al eliminar el registro: " + ex.Message);
            }

        }

        private void dgvPersonas_DoubleClick(object sender, EventArgs e)
        {
            IDregistro = 0;
            IDregistro = Convert.ToInt32(dgvPersonas.CurrentRow.Cells[0].Value);
            txtNombre.Text = dgvPersonas.CurrentRow.Cells[1].Value.ToString();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (IDregistro > 0)
            {
                SQLiteConnection conn = new SQLiteConnection(conexion);
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                SQLiteCommand cmd = new SQLiteCommand("UPDATE PERSONAS SET NOMBRE = @NOMBRE WHERE ID = @CODIGO", conn);
                cmd.Parameters.AddWithValue("CODIGO", IDregistro);
                cmd.Parameters.AddWithValue("NOMBRE", txtNombre.Text.Trim());
                try
                {
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Registro actualizado con éxito!");
                    txtNombre.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al actualizar el registro: " + ex.Message);
                }

                Cargar();

            }
        }
    }
}
